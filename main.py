from functions import m_json
from functions import m_pck

path = "datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(path)


folder_path="datasheets"
m_json.add_temperature_sensor_serials(folder_path, metadata)



measurment_dict = m_pck.get_meas_data_calorimetry(metadata)

data_folder= "data/newton_data"

m_pck.logging_calorimetry( measurment_dict, metadata, data_folder,folder_path)

m_json.archiv_json(folder_path, path, data_folder)